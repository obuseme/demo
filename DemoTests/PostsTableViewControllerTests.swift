//
//  PostsTableViewControllerTests.swift
//  DemoTests
//
//  Created by Andy Obusek on 8/21/17.
//  Copyright © 2017 ao. All rights reserved.
//

import XCTest
@testable import Demo

class PostsTableViewControllerTests: XCTestCase {
    
    /// This test demonstrates a really nice technique that I like for verifying that a method is called
    /// on a specific object.
    func testViewDidLoad_LoadsPosts_And_AddsSelectorToRefreshControl() {
        class MockRefreshControl: UIRefreshControl {
            var addTargetCalled = false

            override func addTarget(_ target: Any?, action: Selector, for controlEvents: UIControlEvents) {
                addTargetCalled = true
            }
        }
        class MockPostsTableViewController: PostsTableViewController {
            let mockRefreshControl = MockRefreshControl()

            override var refreshControl: UIRefreshControl? {
                get {
                    return mockRefreshControl
                }
                set {
                    // no op
                }
            }

            var loadPostsCalled = false

            override func loadPosts() {
                loadPostsCalled = true
            }
        }

        let toTest = MockPostsTableViewController()
        toTest.viewDidLoad()
        XCTAssertTrue(toTest.mockRefreshControl.addTargetCalled)
        XCTAssertTrue(toTest.loadPostsCalled)
    }
    
}

//
//  PostsTableViewController.swift
//  DemoTVApp
//
//  Created by Andy Obusek on 8/20/17.
//  Copyright © 2017 ao. All rights reserved.
//

import UIKit
import APILoadersTVOS

class PostsTableViewController: UITableViewController {

    var posts: [Post] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        loadPosts()
    }

    @objc func loadPosts() {
        PostLoader.load { (posts) in
            if let posts = posts {
                self.posts = posts
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostTableViewCell

        cell.titleLabel.text = posts[indexPath.row].title

        return cell
    }
}

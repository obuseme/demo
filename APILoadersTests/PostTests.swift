//
//  PostTests.swift
//  APILoadersTests
//
//  Created by Andy Obusek on 8/21/17.
//  Copyright © 2017 ao. All rights reserved.
//

import XCTest
@testable import APILoaders

class PostTests: XCTestCase {

    func testParsing() {
        let json = """
            [
                {
                    "userId": 1,
                    "id": 1,
                    "title": "a title",
                    "body": "a body"
                }
            ]
        """.replacingOccurrences(of: "\n", with: " ").data(using: .utf8)!
        let posts = try! JSONDecoder().decode([Post].self, from: json)
        XCTAssertEqual(1, posts.count)
        let firstPost = posts[0]
        XCTAssertEqual(1, firstPost.userId)
        XCTAssertEqual(1, firstPost.id)
        XCTAssertEqual("a title", firstPost.title)
        XCTAssertEqual("a body", firstPost.body)
    }
    
}

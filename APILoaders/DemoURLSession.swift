//
//  DemoURLSession.swift
//  APILoaders
//
//  Created by Andy Obusek on 8/20/17.
//  Copyright © 2017 ao. All rights reserved.
//

import Foundation

public class DemoURLSession {
    public static let shared = DemoURLSession()
    public let session = URLSession(configuration: .default)

    private init() {

    }
}

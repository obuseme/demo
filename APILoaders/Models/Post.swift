//
//  Post.swift
//  APILoaders
//
//  Created by Andy Obusek on 8/20/17.
//  Copyright © 2017 ao. All rights reserved.
//

import Foundation
import CoreData

/// Represents a Post from https://jsonplaceholder.typicode.com/
public struct Post: Decodable {
    public let userId: Int
    public let id: Int
    public let title: String
    public let body: String

    init(coreDataPost: CoreDataPost) {
        self.id = Int(coreDataPost.id)
        self.body = coreDataPost.body ?? ""
        self.userId = Int(coreDataPost.userId)
        self.title = coreDataPost.title ?? ""
    }
}

/// Loads Posts from https://jsonplaceholder.typicode.com/
public struct PostLoader {

    /// Loads the initial page of Posts from https://jsonplaceholder.typicode.com/
    ///
    /// - Parameter completion: Called upon completion of loading Posts. Array of Posts if successful, nil otherwise
    public static func load(completion: (([Post]?) -> Void)!) {
        load(page: 1, completion: completion)
    }

    /// Loads the specified page of Posts from https://jsonplaceholder.typicode.com/
    ///
    /// - Parameters:
    ///   - page: The page to load
    ///   - completion: Called upon completion of loading Posts. Array of Posts if successful, nil otherwise
    public static func load(page: Int,  completion: (([Post]?) -> Void)!) {
        loadPostsFromCoreData(completion)

        let urlString = "https://jsonplaceholder.typicode.com/posts"
        guard let url = URL(string: urlString) else {
            return
        }
        DemoURLSession.shared.session.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                completion(nil)
                return
            }
            do {
                let posts = try JSONDecoder().decode([Post].self, from: data)
                saveLocally(posts: posts)
                completion(posts)
            } catch {
                completion(nil)
            }
        }.resume()
    }

    // MARK: - Private

    fileprivate static func saveLocally(posts: [Post]) {
        let backgroundContext = CoreDataStack.shared.persistentContainer.newBackgroundContext()
        for post in posts {
            let coreDataPost = CoreDataPost(context: backgroundContext)
            coreDataPost.id = Int16(post.id)
            coreDataPost.body = post.body
            coreDataPost.title = post.title
            coreDataPost.userId = Int16(post.userId)
        }
        try? backgroundContext.save()
    }

    fileprivate static func loadPostsFromCoreData(_ completion: (([Post]?) -> Void)!) {
        let request: NSFetchRequest<CoreDataPost> = CoreDataPost.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]

        do {
            let fetchedPosts = try CoreDataStack.shared.persistentContainer.viewContext.fetch(request)
            var postsFromDisk:[Post] = []
            for coreDataPost in fetchedPosts {
                postsFromDisk.append(Post(coreDataPost: coreDataPost))
            }
            completion(postsFromDisk)
        } catch {
            // no posts found locally
        }
    }
}

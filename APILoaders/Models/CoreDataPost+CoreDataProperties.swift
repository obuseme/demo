//
//  CoreDataPost+CoreDataProperties.swift
//  
//
//  Created by Andy Obusek on 8/21/17.
//
//

import Foundation
import CoreData


extension CoreDataPost {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CoreDataPost> {
        return NSFetchRequest<CoreDataPost>(entityName: "CoreDataPost")
    }

    @NSManaged public var body: String?
    @NSManaged public var id: Int16
    @NSManaged public var title: String?
    @NSManaged public var userId: Int16

}

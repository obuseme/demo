//
//  PostTableViewCell.swift
//  Demo
//
//  Created by Andy Obusek on 8/20/17.
//  Copyright © 2017 ao. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!

}

Demo App
------

[![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=599a27052ce63a0001149413&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/599a27052ce63a0001149413/build/latest?branch=master)

**Note: ** This project requires Xcode 9 (built with beta 5)

## Covered Technologies
- Swift 4, iOS 11
- buddybuild integration
- Asynchronous retrieval of Post data from endpoint via URLSession
- Singleton pattern for managing a single URLSession object
- iOS 11 JSON parsing via Decodable protocol
- Decoupling of API interaction and parsing code into a Framework
- Basic tvOS app
- Use of Cocoapods for Chameleon framework
- Documentation on key framework objects
- Include UIRefreshControl for pull to refresh
- Nice app icon and splash screen
- Use of storyboard and Auto Layout for building the user interface
- GCD to ensure that UI updates are performed on the main thread
- CoreData - Saving and Querying in iOS 11 CoreData stack for rudimentary offline support
- Self sizing tableviewcells
- Unit tests for JSON parsing code
- git history that demonstrates a workflow of small, atomic commits

## TODO
- Adaptive layout for tableviewcells
- AVFoundation for processing of Live Photos for a post?

Attribution
------
App logo made by [freepik](https://www.flaticon.com/authors/freepik)
